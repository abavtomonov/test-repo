# -*- coding: utf-8 -*-
import dataiku
import pandas as pd, numpy as np
from dataiku import pandasutils as pdu

# Read recipe inputs
result_sale_flat = dataiku.Dataset("RESULT_sale_flat")
result_sale_flat_df = result_sale_flat.get_dataframe()


# Compute recipe outputs from inputs
# TODO: Replace this part by your actual code that computes the output, as a Pandas dataframe
# NB: DSS also supports other kinds of APIs for reading and writing data. Please see doc.

test_py_df = result_sale_flat_df # For this sample code, simply copy input to output

# HELLOWORLD
# Write recipe outputs
test_py = dataiku.Dataset("test-py")
test_py.write_with_schema(test_py_df)
